<p align="center">
    <a href="https://laravel.com/" target="_blank">
        <img src="https://laravel.com/assets/img/components/logo-laravel.svg">
    </a>
    <a href="https://eshop.slavapleshkov.com" target="_blank">
        <img src="https://eshop.slavapleshkov.com/icons/mstile-310x310.png" height="100px">
    </a>
    <h1 align="center">laravel-e-shop</h1>
    <br>
</p>
<p align="center">The online store was created using the Laravel Framework</p>
